# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def CaloRingsAsymElectronBuilderCfg(flags, name="CaloRingsAsymBuilder", **kwargs):
    acc = ComponentAccumulator()
    kwargs['EtaWidth'] = [0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.2]
    kwargs['PhiWidth'] = [0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170, 0.098174770424681, 0.098174770424681, 0.098174770424681]
    NRings = [8, 64, 8, 8, 4, 4, 4]
    kwargs['NRings'] = [(rings-1)*4+1 for rings in obj.NRings]
    kargs['doEtaAxesDivision'] = True
    kargs['doPhiAxesDivision'] = True
    kwargs['CellMaxDEtaDist'] = .2
    kwargs['CellMaxDPhiDist'] = .2
    from ROOT import CaloCell_ID
    kwargs['Layers'] = [CaloCell_ID.PreSamplerB, CaloCell_ID.PreSamplerE,
                   CaloCell_ID.EMB1,        CaloCell_ID.EME1,
                   CaloCell_ID.EMB2,        CaloCell_ID.EME2,
                   CaloCell_ID.EMB3,        CaloCell_ID.EME3,
                   CaloCell_ID.HEC0,        CaloCell_ID.TileBar0,    CaloCell_ID.TileGap3, CaloCell_ID.TileExt0,
                   CaloCell_ID.HEC1,        CaloCell_ID.HEC2,        CaloCell_ID.TileBar1, CaloCell_ID.TileGap1, CaloCell_ID.TileExt1,
                   CaloCell_ID.HEC3,        CaloCell_ID.TileBar2,    CaloCell_ID.TileGap2, CaloCell_ID.TileExt2]
    kwargs['RingSetNLayers'] = [2, 2, 2, 2, 4, 5, 4]
    kwargs['useShowerShapeBarycenter'] = False
    kwargs['CellsContainerName'] = egammaKeys.caloCellKey()
    kwargs['CaloRingsContainerName'] = 'ElectronCaloRings'
    kwargs['RingSetContainerName'] = 'ElectronRingSets'
    from AthenaCommon.SystemOfUnits import GeV
    kwargs['MinPartEnergy'] = 15*GeV
    tool = CompFactory.Ringer.CaloRingsAsymBuilder(name, **kwargs)
    acc.setPrivateTools(tool)
    return acc

def CaloRingsAsymPhotonBuilderCfg(flags, name="CaloRingsAsymBuilder", **kwargs):
    acc = ComponentAccumulator()
    kwargs['EtaWidth'] = [0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.2]
    kwargs['PhiWidth'] = [0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170, 0.098174770424681, 0.098174770424681, 0.098174770424681]
    NRings = [8, 64, 8, 8, 4, 4, 4]
    kwargs['NRings'] = [(rings-1)*4+1 for rings in obj.NRings]
    kargs['doEtaAxesDivision'] = True
    kargs['doPhiAxesDivision'] = True
    kwargs['CellMaxDEtaDist'] = .2
    kwargs['CellMaxDPhiDist'] = .2
    from ROOT import CaloCell_ID
    kwargs['Layers'] = [CaloCell_ID.PreSamplerB, CaloCell_ID.PreSamplerE,
                   CaloCell_ID.EMB1,        CaloCell_ID.EME1,
                   CaloCell_ID.EMB2,        CaloCell_ID.EME2,
                   CaloCell_ID.EMB3,        CaloCell_ID.EME3,
                   CaloCell_ID.HEC0,        CaloCell_ID.TileBar0,    CaloCell_ID.TileGap3, CaloCell_ID.TileExt0,
                   CaloCell_ID.HEC1,        CaloCell_ID.HEC2,        CaloCell_ID.TileBar1, CaloCell_ID.TileGap1, CaloCell_ID.TileExt1,
                   CaloCell_ID.HEC3,        CaloCell_ID.TileBar2,    CaloCell_ID.TileGap2, CaloCell_ID.TileExt2]
    kwargs['RingSetNLayers'] = [2, 2, 2, 2, 4, 5, 4]
    kwargs['useShowerShapeBarycenter'] = False
    kwargs['CellsContainerName'] = egammaKeys.caloCellKey()
    kwargs['CaloRingsContainerName'] = 'PhotonCaloRings'
    kwargs['RingSetContainerName'] = 'PhotonRingSets'
    from AthenaCommon.SystemOfUnits import GeV
    kwargs['MinPartEnergy'] = 15*GeV
    tool = CompFactory.Ringer.CaloRingsAsymBuilder(name, **kwargs)
    acc.setPrivateTools(tool)
    return acc

def CaloRingsElectronBuilderCfg(flags, name="CaloRingsElectronBuilder", **kwargs):
    from egammaRec import egammaKeys
    acc = ComponentAccumulator()
    kwargs['EtaWidth'] = [0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.2]
    kwargs['PhiWidth'] = [0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170, 0.098174770424681, 0.098174770424681, 0.098174770424681]
    kwargs['NRings'] = [8, 64, 8, 8, 4, 4, 4]
    kwargs['CellMaxDEtaDist'] = .2
    kwargs['CellMaxDPhiDist'] = .2
    from ROOT import CaloCell_ID
    kwargs['Layers'] = [CaloCell_ID.PreSamplerB, CaloCell_ID.PreSamplerE,
                   CaloCell_ID.EMB1,        CaloCell_ID.EME1,
                   CaloCell_ID.EMB2,        CaloCell_ID.EME2,
                   CaloCell_ID.EMB3,        CaloCell_ID.EME3,
                   CaloCell_ID.HEC0,        CaloCell_ID.TileBar0,    CaloCell_ID.TileGap3, CaloCell_ID.TileExt0,
                   CaloCell_ID.HEC1,        CaloCell_ID.HEC2,        CaloCell_ID.TileBar1, CaloCell_ID.TileGap1, CaloCell_ID.TileExt1,
                   CaloCell_ID.HEC3,        CaloCell_ID.TileBar2,    CaloCell_ID.TileGap2, CaloCell_ID.TileExt2]
    kwargs['RingSetNLayers'] = [2, 2, 2, 2, 4, 5, 4]
    kwargs['useShowerShapeBarycenter'] = False
    kwargs['CellsContainerName'] = egammaKeys.caloCellKey()
    kwargs['CaloRingsContainerName'] = 'ElectronCaloRings'
    kwargs['RingSetContainerName'] = 'ElectronRingSets'
    from AthenaCommon.SystemOfUnits import GeV
    kwargs['MinPartEnergy'] = 15*GeV
    tool = CompFactory.Ringer.CaloRingsBuilder(name, **kwargs)
    acc.setPrivateTools(tool)
    return acc

def CaloRingsPhotonBuilderCfg(flags, name="CaloRingsPhotonBuilder", **kwargs):
    from egammaRec import egammaKeys
    acc = ComponentAccumulator()
    kwargs['RingSetContainerName'] = 'EgammaRingSets'
    kwargs['CaloRingsContainerName'] = 'EgammaCaloRings'
    kwargs['EtaWidth'] = [0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.2]
    kwargs['PhiWidth'] = [0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170, 0.098174770424681, 0.098174770424681, 0.098174770424681]
    kwargs['NRings'] = [8, 64, 8, 8, 4, 4, 4]
    kwargs['CellMaxDEtaDist'] = .2
    kwargs['CellMaxDPhiDist'] = .2
    from ROOT import CaloCell_ID
    kwargs['Layers'] = [CaloCell_ID.PreSamplerB, CaloCell_ID.PreSamplerE,
                   CaloCell_ID.EMB1,        CaloCell_ID.EME1,
                   CaloCell_ID.EMB2,        CaloCell_ID.EME2,
                   CaloCell_ID.EMB3,        CaloCell_ID.EME3,
                   CaloCell_ID.HEC0,        CaloCell_ID.TileBar0,    CaloCell_ID.TileGap3, CaloCell_ID.TileExt0,
                   CaloCell_ID.HEC1,        CaloCell_ID.HEC2,        CaloCell_ID.TileBar1, CaloCell_ID.TileGap1, CaloCell_ID.TileExt1,
                   CaloCell_ID.HEC3,        CaloCell_ID.TileBar2,    CaloCell_ID.TileGap2, CaloCell_ID.TileExt2]
    kwargs['RingSetNLayers'] = [2, 2, 2, 2, 4, 5, 4]
    kwargs['useShowerShapeBarycenter'] = False
    kwargs['CellsContainerName'] = egammaKeys.caloCellKey()
    kwargs['CaloRingsContainerName'] = 'PhotonCaloRings'
    kwargs['RingSetContainerName'] = 'PhotonRingSets'
    from AthenaCommon.SystemOfUnits import GeV
    kwargs['MinPartEnergy'] = 15*GeV
    tool = CompFactory.Ringer.CaloRingsBuilder(name, **kwargs)
    acc.setPrivateTools(tool)
    return acc

# def CaloRingerInputReaderCfg(flags, name="CaloRingerInputReader", **kwargs):
#     acc = ComponentAccumulator()
#     if 'crBuilder' not in kwargs:
#         crBuilder = CaloRingsBuilderCfg(flags)
#         kwargs['crBuilder'] = acc.popToolsAndMerge(crBuilder)
    
#     tool = CompFactory.Ringer.CaloRingerInputReader(name, **kwargs)
#     acc.setPrivateTools(tool)
#     return acc

def CaloRingerElectronsInputReaderCfg(flags, name="CaloRingerElectronsInputReader", **kwargs):
    acc = ComponentAccumulator()
    if 'crBuilder' not in kwargs:
        # if doStandard:
        #     crBuilder = CaloRingsBuilderCfg(flags)
        # if doAsym:
        #     crBuilder = CaloRingsAsymBuilderCfg(flags)
        crBuilder = CaloRingsElectronBuilderCfg(flags)
        kwargs['crBuilder'] = acc.popToolsAndMerge(crBuilder)
        kwargs['inputKey'] = 'Electrons'
    tool = CompFactory.Ringer.CaloRingerElectronsReader(name,**kwargs)
    acc.setPrivateTools(tool)
    return acc

def CaloRingerPhotonsInputReaderCfg(flags, name="CaloRingerPhotonsInputReader", **kwargs):
    acc = ComponentAccumulator()
    if 'crBuilder' not in kwargs:
        # if doStandard:
        #     crBuilder = CaloRingsBuilderCfg(flags)
        # if doAsym:
        #     crBuilder = CaloRingsAsymBuilderCfg(flags)
        crBuilder = CaloRingsPhotonBuilderCfg(flags)
        kwargs['crBuilder'] = acc.popToolsAndMerge(crBuilder)
        kwargs['inputKey'] = 'Photons'

    tool = CompFactory.Ringer.CaloRingerPhotonsReader(name,**kwargs)
    acc.setPrivateTools(tool)
    return acc

def CaloRingerAlgsCfg(flags, name="CaloRingerAlgorithm", **kwargs):
    """Configure the CaloRingerAlgs."""
    acc = ComponentAccumulator()
    toolList = [CaloRingerElectronsInputReaderCfg(flags).getPrimary(), CaloRingerPhotonsInputReaderCfg(flags).getPrimary()]
    # if doElectron:
    #     toolList.append(CaloRingerElectronsInputReaderCfg(flags).getPrimary())
    # if doPhoton:
    #     tool
    if 'inputReaderTools' not in kwargs:
        kwargs['inputReaderTools'] = toolList
    
    CaloRingerAlgorithm = CompFactory.Ringer.CaloRingerAlgorithm(name, **kwargs)
   
    acc.addEventAlgo(CaloRingerAlgorithm)
    return acc

if __name__ == '__main__':
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = 1
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    from AthenaConfiguration.TestDefaults import defaultTestFiles
    ConfigFlags.Input.Files = defaultTestFiles.RAW
    ConfigFlags.lock()

    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)


    from CaloRingerAlgs.CaloRingerAlgsConfig import CaloRingerAlgsCfg
    cfg.merge(CaloRingerAlgsCfg(ConfigFlags))